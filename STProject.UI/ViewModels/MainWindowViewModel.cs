﻿

using Caliburn.Micro;
using ExcelDataReader;
using System;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace STProject.UI.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        #region Private Fields
        private string _excelFilePath = "";
        private string _excelFileName = "";
        private DataSet _ds;
        private DataView _excelContent;
        #endregion


        #region Public Props
        public string ExcelFileName
        {
            get { return _excelFileName; }
            set { _excelFileName = value; NotifyOfPropertyChange(() => ExcelFileName); }
        }

        public DataView ExcelContent
        {
            get { return _excelContent; }
            set { _excelContent = value; NotifyOfPropertyChange(() => ExcelContent); }
        }

        #endregion

        #region Commands
        public void ImportExcelCommand() => ImportExcel();
        public void ReloadExcelCommand() => ReloadExcel();

        #endregion

        #region Ctor

        public MainWindowViewModel()
        {
            
        }

        #endregion





        

        /// <summary>
        /// Open  dilog in order to select excel file
        /// </summary>
        private void ImportExcel()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel Files (*.xlsx)|*.xlsx|CSV Files (*.csv)|*.csv|Old Excel Files (*.xls)|*.xls";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                _excelFilePath = filename;
                ExcelFileName = Path.GetFileName(filename);
                LoadExcelFile();
            }
        }

        /// <summary>
        /// Load excel file as stream
        /// </summary>
        private async void LoadExcelFile()
        {
            await Task.Run(() =>
            {
                var extension = Path.GetExtension(_excelFilePath).ToLower();
                using (var stream = new FileStream(_excelFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    IExcelDataReader reader = null;
                    if (extension == ".xls") // file is old excel file
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (extension == ".xlsx")// file is old excel file (2007 onwards)
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else if (extension == ".csv") // file is old comma separated value
                    {
                        reader = ExcelReaderFactory.CreateCsvReader(stream);
                    }

                    // file is not acceptale
                    if (reader == null)
                    {
                        MessageBox.Show("file is not acceptable");
                        return;
                    }


                    using (reader)
                    {
                        _ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            UseColumnDataType = false,
                            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true
                            }
                        });
                        
                    }
                }
            });
            ExcelContent = _ds.Tables[0].DefaultView;
        }

        private void ReloadExcel()
        {
            
        }
    }
}
