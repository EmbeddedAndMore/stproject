﻿

using System.Windows;
using Caliburn.Micro;
using STProject.UI.ViewModels;

namespace STProject.UI
{
    public class Bootstrpper : BootstrapperBase
    {
        #region Ctor
        public Bootstrpper()
        {
            Initialize();
        }
        #endregion

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
    }
}
